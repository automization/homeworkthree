package ru.rtkit.petstore.apiHelper;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;

import java.util.Properties;

import static io.restassured.RestAssured.given;
import static ru.rtkit.petstore.tests.BaseTest.props;

public class PetstoreHelper {

    private final String URI = props.getProperty("URL");

    public PetstoreHelper() {
        RestAssured.baseURI = URI;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.filters(new AllureRestAssured());
    }


    public Response get(String endpoint, ResponseSpecification resp, Object pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint, pathParams);
    }

    public Response post(String endpoint, Object body, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }

    public Response delete(String endpoint, ResponseSpecification resp, Object pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .delete(endpoint, pathParams);
    }
}
