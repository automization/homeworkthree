package ru.rtkit.petstore.apiHelper;

public class Endpoints {

    public static final String NEW_PET = "/pet";
    public static final String PET_ID = "/pet/{petId}";

    public static final String ORDER = "/store/order";
    public static final String ORDER_ID = "/store/order/{orderId}";

}
