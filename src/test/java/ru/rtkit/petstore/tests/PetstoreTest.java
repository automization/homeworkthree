package ru.rtkit.petstore.tests;

import io.restassured.path.json.JsonPath;
import jdk.jfr.Description;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;
import ru.rtkit.petstore.apiHelper.Endpoints;
import ru.rtkit.petstore.model.newCat.Cat;
import ru.rtkit.petstore.model.order.Order;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test Petstore")
public class PetstoreTest extends BaseTest {

    @DisplayName("Checking created pets")
    @Description("Testing of creating and finding pets")
    @ParameterizedTest(name = "{index} -> checking ''{0}''")
    @CsvFileSource(files = "nameOfCats.csv")
    void checkingNewCats(String nameOfCat) {

        int id = 36;

        Cat catRequest = new Cat();
        catRequest.setId(id);
        catRequest.setName(nameOfCat);
        catRequest.setStatus("available");

        petstoreHelper.post(Endpoints.NEW_PET, catRequest, resp200);

        petstoreHelper.get(Endpoints.PET_ID, resp200, id)
                .then()
                .assertThat()
                .body("id", Matchers.equalTo(id))
                .body("name", Matchers.equalTo(nameOfCat))
                .body("photoUrls", Matchers.empty())
                .body("status", Matchers.equalTo("available"));

        petstoreHelper.delete(Endpoints.PET_ID, resp200, id);

        petstoreHelper.get(Endpoints.PET_ID, resp404, id)
                .then()
                .assertThat()
                .body("code", Matchers.equalTo(1))
                .body("type", Matchers.equalTo("error"))
                .body("message", Matchers.notNullValue());
    }

    @DisplayName("Checking created orders")
    @Description("Testing of creating orders")
    @ParameterizedTest(name = "{index} -> checking ''{0}''")
    @ValueSource(strings = {"placed", "approved", "delivered"})
    void checkingNewOrders(String status) {

        int id = 100;
        int orderId = 1;
        Date dateNow = new Date();
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");

        Cat catRequest = new Cat();
        catRequest.setId(id);
        catRequest.setName("Буся");
        catRequest.setStatus("available");

        Order orderRequest = new Order();
        orderRequest.setId(orderId);
        orderRequest.setPetId(id);
        orderRequest.setQuantity(1);
        orderRequest.setShipDate(newFormat.format(dateNow));
        orderRequest.setStatus(status);
        orderRequest.setComplete(true);

        petstoreHelper.post(Endpoints.NEW_PET, catRequest, resp200)
                .then()
                .body(containsString("Буся"));

        JsonPath response = petstoreHelper.post(Endpoints.ORDER, orderRequest, resp200)
                .then()
                .extract()
                .body()
                .jsonPath();

        assertEquals(response.getInt("petId"), id, "Ошибка при проверке присвоенного id");
        assertEquals(response.getString("status"), status, "Ошибка при проверке статуса");
        assertTrue(response.getBoolean("complete"), "Ошибка при проверке доступности животного");
        assertNotNull(response.getString("shipDate"), "Ошибка при проверке времени");

        petstoreHelper.delete(Endpoints.ORDER_ID, resp200, orderId);
        petstoreHelper.delete(Endpoints.PET_ID, resp200, id);
    }
}